using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using ByondServerStatusApi.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

namespace ByondServerStatusApi
{
    [Route("[controller]")]
    [ApiController]
    public class ApiController : ControllerBase
    {
        private readonly IServerData _serverData;

        public ApiController(IServerData data)
        {
            _serverData = data;
        }
        
        
        [HttpGet]
        public ICollection<string> GetServerList()
        {
            return _serverData.Get().Keys;
        }
        
        [HttpGet("{id}")]
        public ActionResult<ConcurrentDictionary<string, StringValues>> GetServerStatus(string id)
        {
            if (_serverData.Get().TryGetValue(id, out var data))
            {
                return data;
            }
            else
            {
                return NotFound();
            }
            
        }
    }
}