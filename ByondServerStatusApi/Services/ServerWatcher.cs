using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ByondServerStatusApi.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;

namespace ByondServerStatusApi.Services
{
    public class ServerWatcher
    {
        private IServerData data;
        private string serverId;
        private string host;
        private int port;

        private static readonly byte[] payload;

        private const string command = "?status";

        private Timer timer;
        private ILogger logger;

        public ServerWatcher(ILogger<ServerWatcher> logger)
        {            
            this.logger = logger;
            //Task.Factory.StartNew(WatchServer);
        }

        public void Init(IServerData data, string serverId, string host, int port)
        {
            this.data = data;
            this.serverId = serverId;
            this.host = host;
            this.port = port;
            this.data.Get().TryAdd(this.serverId, new ConcurrentDictionary<string, StringValues>());
            
            timer = new Timer(state =>
            {
                WatchServer(state);
                timer.Change(1000, Timeout.Infinite);
            }, null, 0, Timeout.Infinite);
        }

        static ServerWatcher()
        {
            var header = new byte[] {0x00, 0x83};

            var lengthBytes = BitConverter.GetBytes((short) command.Length + 6);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(lengthBytes);
            }

            var commandBytes = Encoding.ASCII.GetBytes(command);
            
            // [0, 131, 0, 13, 0, 0, 0, 0, 0, 63, 115, 116, 97, 116, 117, 115, 0]

            payload = header.Concat(lengthBytes.Skip(2)).Concat(new byte[5]).Concat(commandBytes).Concat(new byte[1])
                .ToArray();
        }

        private void WatchServer(object state)
        {
            this.logger.LogDebug($"Watcher {this.serverId} fetching status");
            
            var buffer = new byte[1024];

            using (var client = new TcpClient())
            {
                
                try
                {
                    client.Connect(this.host, this.port);
                }
                catch (Exception e)
                {
                    this.data.Get()[serverId].Clear();
                    this.logger.LogError($"Watcher {this.serverId} threw exception {e.Message}", e);
                    return;
                }

                using (var stream = client.GetStream())
                {
                    stream.Write(payload, 0, payload.Length);

                    Array.Clear(buffer, 0, buffer.Length);

                    stream.Read(buffer);

                    if (buffer[0] != 0x00 || buffer[1] != 0x83)
                    {
                        this.data.Get()[serverId].Clear();
                        stream.Close();
                        this.logger.LogError($"Watcher {this.serverId} encountered invalid response header");
                        return;
                    }

                    var dataSize = BitConverter.ToUInt16(buffer.Skip(2).Take(2).Reverse().ToArray(), 0) - 2;

                    var response = Encoding.ASCII.GetString(buffer, 5, dataSize);

                    this.logger.LogDebug($"Watcher {this.serverId} got data from server successfully.");

                    var values = Microsoft.AspNetCore.WebUtilities.QueryHelpers.ParseQuery(response);

                    this.data.Get()[serverId] = new ConcurrentDictionary<string, StringValues>(values);
                }
            }
        }
    }
}