using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using ByondServerStatusApi.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;

namespace ByondServerStatusApi.Services
{
    public class ServerData : IServerData
    {
        private readonly ConcurrentDictionary<string, ConcurrentDictionary<string, StringValues>> data;
        private readonly List<ServerWatcher> ServerWatchers;

        public ServerData(IServiceProvider serviceProvider, IConfiguration config)
        {
            List<ServerWatcher> serverWatchers1;
            List<ServerWatcher> serverWatchers;
            this.data = new ConcurrentDictionary<string, ConcurrentDictionary<string, StringValues>>();
            this.ServerWatchers = new List<ServerWatcher>();

            foreach (var server in config.GetSection("Servers").GetChildren())
            { 
                var host = server.GetValue<string>("host");
                var port = server.GetValue<int>("port");
                
                var watcher = new ServerWatcher((ILogger<ServerWatcher>)serviceProvider.GetService(typeof(ILogger<ServerWatcher>)));
                watcher.Init(this, server.Key, host, port);
                this.ServerWatchers.Add(watcher);
            }
        }
        
        public ConcurrentDictionary<string, ConcurrentDictionary<string, StringValues>> Get()
        {
            return data;
        }
    }
}