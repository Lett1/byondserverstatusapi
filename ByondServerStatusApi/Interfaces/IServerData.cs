using System.Collections.Concurrent;
using Microsoft.Extensions.Primitives;

namespace ByondServerStatusApi.Interfaces
{
    public interface IServerData
    {
        ConcurrentDictionary<string, ConcurrentDictionary<string, StringValues>> Get();
    }
}